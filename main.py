import os
import time
import logging
import subprocess
from PIL import Image
from config import MAIN_STEP, SECOND_STEP, OFFSET, BACKGROUND_COLOR, START_DIRECTORY, TARGET_DIRECTORY, CORRECT_COEF, \
    BREAK_FILE_LOG_ADDRESS

logger = logging.getLogger('imager')
logger.setLevel('DEBUG')
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
handler.setFormatter(logging.Formatter(
    '%(asctime)s %(levelname)s: %(message)s ', "%Y-%m-%d %H:%M:%S"
))
logger.addHandler(handler)


def get_color_borders(im):
    corners = map(im.getpixel,
                  [(0, 0), (im.size[0] - 1, 0), (im.size[0] - 1, im.size[1] - 1), (0, im.size[1] - 1)])
    colors = zip(*corners)
    names = dict(zip(['red', 'blue', 'green'], colors))
    result = {}
    for n in names:
        r = {'max': max(names[n]) + 10,
             'min': min(names[n]) - 10}
        result[n] = r
    return result


def in_borders(color, color_borders):
    names = ['red', 'blue', 'green']
    for i in xrange(len(color)):
        if color[i] <= color_borders[names[i]]['min'] or color[i] >= color_borders[names[i]]['max']:
            return False
    return True


def get_margin(im, direction, color_borders):
    param = {
        'left': {'start_point_main': 0,
                 'end_point_main': im.size[0],
                 'start_point_second': 0,
                 'end_point_second': im.size[1],
                 'step_main': MAIN_STEP,
                 'step_second': SECOND_STEP},
        'right': {'start_point_main': im.size[0] - 1,
                  'end_point_main': 0,
                  'start_point_second': 0,
                  'end_point_second': im.size[1],
                  'step_main': -MAIN_STEP,
                  'step_second': SECOND_STEP},
        'top': {'start_point_main': 0,
                'end_point_main': im.size[1],
                'start_point_second': 0,
                'end_point_second': im.size[0],
                'step_main': MAIN_STEP,
                'step_second': SECOND_STEP},
        'bottom': {'start_point_main': im.size[1] - 1,
                   'end_point_main': 0,
                   'start_point_second': 0,
                   'end_point_second': im.size[0],
                   'step_main': -MAIN_STEP,
                   'step_second': SECOND_STEP}
    }
    p = param[direction]
    result = None
    for main in xrange(p['start_point_main'], p['end_point_main'], p['step_main']):
        for second in xrange(p['start_point_second'], p['end_point_second'], p['step_second']):
            if direction in ['left', 'right']:
                x, y = main, second
            else:
                x, y = second, main
            if not in_borders(im.getpixel((x, y)), color_borders):
                result = main
                break
        if result:
            break
    if not result:
        return p['start_point_main']
    return result


for root, dirnames, filenames in os.walk(START_DIRECTORY):
    new_path = root.replace(START_DIRECTORY, TARGET_DIRECTORY)
    if not os.path.exists(new_path):
        os.mkdir(new_path)
    counter_mod = 0
    counter_not = 0
    for f in filenames:
        extension = f.lower().split('.')[-1]
        if extension not in ['jpg', 'jpeg', 'png']:
            continue
        root_path, target_path = root + '/' + f, new_path + '/' + f
        if not os.path.exists(target_path) or abs(os.path.getmtime(root_path) - os.path.getmtime(target_path)) > 0.5:
            im = Image.open(root_path)
            if extension in ('png',):
                im = im.convert('RGB')
            try:
                color_borders = get_color_borders(im)
            except IOError:
                logger.error('File {} is truncated'.format(root_path))
                with open(BREAK_FILE_LOG_ADDRESS + '/break_pictures.log', 'a') as f:
                    f.write(root_path + '\n')
                continue
            directions = ['left', 'right', 'top', 'bottom']
            margins = dict(zip(directions, [get_margin(im, d, color_borders) for d in directions]))
            new_pixels = im.crop((margins['left'], margins['top'], margins['right'], margins['bottom'])).getdata()
            if new_pixels.size[0] < CORRECT_COEF*im.size[0] or new_pixels.size[1] < CORRECT_COEF*im.size[1]:
                new_pixels = im
            sizex, sizey = new_pixels.size[0], new_pixels.size[1]
            big_size = max(sizex, sizey) + OFFSET*2
            new_im = Image.new('RGB', (big_size, big_size), BACKGROUND_COLOR)
            offsetx, offsety = (big_size - sizex)/2, (big_size - sizey)/2
            new_im.paste(new_pixels, (offsetx, offsety, offsetx + new_pixels.size[0], offsety + new_pixels.size[1]))
            new_im.save(target_path, "JPEG")
            mod_time = os.path.getmtime(root_path)
            os.utime(target_path, (mod_time, mod_time))
            counter_mod += 1
            subprocess.call(
                '/usr/local/bin/rsync -akvR {root_directory}/./{path} resize05.local::retargeting/'.format(
                    root_directory=TARGET_DIRECTORY,
                    path=target_path.replace(TARGET_DIRECTORY + '/', '')), shell=True)
            subprocess.call(
                '/usr/local/bin/rsync -akvR {root_directory}/./{path} resize06.local::retargeting/'.format(
                    root_directory=TARGET_DIRECTORY,
                    path=target_path.replace(TARGET_DIRECTORY + '/', '')), shell=True)
        else:
            counter_not += 1
    logger.info(
        "Directory '{}', {} files. {} files were modified.".format(
            root, counter_not + counter_mod, counter_mod))
